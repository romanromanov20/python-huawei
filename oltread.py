#!/usr/bin/env python
import paramiko, time, sys, json


def service_func(router, username, password, sn_number):
	stringg = ''
	stringg2 = ''
	stringreg = ''
	stringopt = ''
	services = 'Null value'
	waninfo = 'Null value'
	maininfo = 'Null value'
	version = 'Null value'
	registerinfo = 'Null value'
	optical = 'Null value'
	d = {'service-port': [], 'svlan': [], 'type': [], 'F/S': [], 'port': [], 'cvlan': [], 'RX': [], 'TX': [], 'state': []}
	d_waninfo = {'Index': [], 'Service type': [], 'Connection type': [], 'Connection status': [], 'IP access type': [], 'IP': [], 'Subnet mask': [], 'Default gateway': [], 'Manage VLAN': [], 'Manage priority': [], 'Option60': []}
	d_maininfo = {'ONT-ID': [], 'F/S/P': [], 'Run state': []}
	d_register = {'Index':[], 'SN':[], 'TYPE':[], 'UpTime':[], 'DownTime':[], 'DownCause':[]}
	

	ssh = paramiko.SSHClient()
	ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	ssh.connect(router, username=username, password=password,allow_agent=False, look_for_keys=False)
#	print('Successfully connected to %s' % router)

	remote_conn = ssh.invoke_shell()
	output = remote_conn.recv(1000)
	remote_conn.send('enable\n')
	remote_conn.send('config\n')
	remote_conn.send('scroll 512\n')
	remote_conn.send('display ont info by-sn '+ sn_number + ' \n')
	remote_conn.send(' \n')
	time.sleep(2)
	outpt = remote_conn.recv(9999)
	output = outpt.decode()
	for line in output.split('\n'):
		if "The required ONT does not exist" in line:
			maininfo = 'The required ONT does not exist'
#			print(maininfo)
			return maininfo, services, waninfo
			sys.exit()
		if "ONT-ID" in line:
			str_onuid = line.split(':')
			line_id = str_onuid[1].lstrip().rstrip('\r')
			d_maininfo['ONT-ID'] = line_id
		if "F/S/P" in line:
			str_onuid = line.split(':')
			line2_id = str_onuid[1].lstrip().rstrip('\r')
			slot_id = line2_id.split('/')[1]
			port_id = line2_id.split('/')[2]
			d_maininfo['F/S/P'] = line2_id
		if "Run state" in line:
			str_state = line.split(':')
			run_state = str_state[1].lstrip().rstrip('\r')
			d_maininfo['Run state'] = run_state
			stringg1 =str(d_maininfo)
			stringg =stringg+','+stringg1
	
	stringg = stringg.replace("\'", "\"")
	stringg = stringg.replace(",", " ", 1)
	stringg = '{ "maininfo": [ '+ stringg +'] }'
	maininfo = stringg
	

	try:
		string_serv = ''
#		print('slot id ='+ slot_id + ' port id = ' + port_id + ' ONT ID = '+ line_id)
		remote_conn.send('display service-port board  0/'+ slot_id +' \n')
		remote_conn.send(' \n')
		time.sleep(2)
		remote_conn.send(' \n')
		time.sleep(2)
		service_output = remote_conn.recv(99999)
		output_n = service_output.decode()
		for line in output_n.split('\n'):
			if ' 0/'+ str(slot_id) +' /'+ str(port_id) +'  '+ str(line_id) in line:
				d['service-port']=line.split()[0]
				d['svlan']=line.split()[1]
				d['type']=line.split()[2]
				d['F/S']=line.split()[4]
				d['port']=line.split()[5]
				d['cvlan']=line.split()[9]
				d['RX']=line.split()[10]
				d['TX']=line.split()[11]
				d['state']=line.split()[12]
				string_serv1 =str(d)
				string_serv =string_serv+','+string_serv1	
		string_serv = string_serv.replace("\'", "\"")
		string_serv = string_serv.replace(",", " ", 1)
		string_serv = '{ "services": [ '+ string_serv +'] }'
		services = string_serv					# Assign values json of all service-ports to a 'services' variable

		

		if 'online' in run_state:
			string_wan = ''
			remote_conn.send('display ont wan-info 0/'+ slot_id +' '+ port_id +' '+ line_id +' \n')
			remote_conn.send('\n')
			time.sleep(2)
			wan_output = remote_conn.recv(99999)
			output_m = wan_output.decode()
			for line in output_m.split('\n'):
				if 'Index' in line:
					str_onuid = line.split(':')
					valuex = str_onuid[1].lstrip().rstrip('\r')
					d_waninfo['Index'] = valuex
				if 'Service type' in line:
					str_onuid = line.split(':')
					valuex = str_onuid[1].lstrip().rstrip('\r')
					d_waninfo['Service type'] = valuex
				if 'Connection type' in line:
					str_onuid = line.split(':')
					valuex = str_onuid[1].lstrip().rstrip('\r')
					d_waninfo['Connection type'] = valuex
				if 'Connection status' in line:
					str_onuid = line.split(':')
					valuex = str_onuid[1].lstrip().rstrip('\r')
					d_waninfo['Connection status'] = valuex
				if 'IP access type' in line:
					str_onuid = line.split(':')
					valuex = str_onuid[1].lstrip().rstrip('\r')
					d_waninfo['IP access type'] = valuex
				if 'IP' in line:
					str_onuid = line.split(':')
					valuex = str_onuid[1].lstrip().rstrip('\r')
					d_waninfo['IP'] = valuex
				if 'Subnet mask' in line:
					str_onuid = line.split(':')
					valuex = str_onuid[1].lstrip().rstrip('\r')
					d_waninfo['Subnet mask'] = valuex
				if 'Default gateway' in line:
					str_onuid = line.split(':')
					valuex = str_onuid[1].lstrip().rstrip('\r')
					d_waninfo['Default gateway'] = valuex
				if 'Manage VLAN' in line:
					str_onuid = line.split(':')
					valuex = str_onuid[1].lstrip().rstrip('\r')
					d_waninfo['Manage VLAN'] = valuex
				if 'Manage priority' in line:
					str_onuid = line.split(':')
					valuex = str_onuid[1].lstrip().rstrip('\r')
					d_waninfo['Manage priority'] = valuex
				if 'Option60' in line:
					str_onuid = line.split(':')
					valuex = str_onuid[1].lstrip().rstrip('\r')
					d_waninfo['Option60'] = valuex
					string_wan1 =str(d_waninfo)
					string_wan =string_wan+','+string_wan1
			string_wan = string_wan.replace("\'", "\"")
			string_wan = string_wan.replace(",", " ", 1)
			string_wan = '{ "wan-info": [ '+ string_wan +'] }'

			waninfo = string_wan

			remote_conn.send('display ont version 0 '+ slot_id +' '+ port_id +' '+ line_id +' \n')
			remote_conn.send('\n')
			time.sleep(2)
			wan_output = remote_conn.recv(99999)
			output_m = wan_output.decode()
			for line in output_m.split('\n'):
				if 'Equipment-ID' in line:
					vers=line.split(':')
					vers=vers[1].lstrip().rstrip('\r')
				if 'Main Software Version' in line:
					soft=line.split(':')
					soft=soft[1].lstrip().rstrip('\r')					

			version = '{"version": [ {"Equipment-ID": "'+vers+'", "Software": "'+soft+'" } ] }' 

			
			remote_conn.send('interface gpon 0/'+ slot_id +'\n')
			remote_conn.send('display ont optical-info '+ port_id +' '+ line_id +' \n')
			remote_conn.send('\n')
			remote_conn.send('quit\n')
			time.sleep(2)
			wan1_output = remote_conn.recv(99999)
			output_mn = wan1_output.decode()
			#print(output_mn)
			rxopt=''
			txopt=''
			for line in output_mn.split('\n'):
				if 'Rx optical power(dBm)' in line:
					if 'CATV' in line:
						continue
					rxopt=line.split(':')
					rxopt=rxopt[1].lstrip().rstrip('\r')
					
				if 'Tx optical power(dBm)' in line:
					txopt=line.split(':')
					txopt=txopt[1].lstrip().rstrip('\r')	
			optical = '{"optical-info": [ {"Rx optical power(dBm)": "'+rxopt+'", "Tx optical power(dBm)": "'+txopt+'" } ] }'
			
		
		
		remote_conn.send('interface gpon 0/'+ slot_id +'\n')

		remote_conn.send('display ont register-info '+ port_id +' '+ line_id +' \n')
		remote_conn.send('\n')
		time.sleep(2)
		wan_output = remote_conn.recv(99999)
		output_m = wan_output.decode()
		for line in output_m.split('\n'):
			if 'Index' in line:
				index=line.split(':')
				d_register['Index']=index[1].lstrip().rstrip('\r')        
			if 'SN' in line:
				sn=line.split(':')
				d_register['SN']=sn[1].lstrip().rstrip('\r')
			if 'TYPE' in line:
				type=line.split(':')
				d_register['TYPE']=type[1].lstrip().rstrip('\r')
			if 'UpTime' in line:
				uptime=line.split(':')
				d_register['UpTime']=uptime[1].lstrip().rstrip('\r')
			if 'DownTime' in line:
				downtime=line.split(':')
				d_register['DownTime']=downtime[1].lstrip().rstrip('\r')
			if 'DownCause' in line:
				downcase=line.split(':')
				d_register['DownCause']=downcase[1].lstrip().rstrip('\r')
				stringreg1 =str(d_register)
				stringreg =stringreg+','+stringreg1
		stringreg = stringreg.replace("\'", "\"")
		stringreg = stringreg.replace(",", " ", 1)
		stringreg = '{ "register-info": [ '+ stringreg +'] }' 
		registerinfo = stringreg

	except Exception as e:
		print(e)

#	print(maininfo) 
#	print(services) 
	print(version)
	return maininfo, services, waninfo, version, registerinfo, optical
	
	ssh.close()



if __name__ == '__main__':
	service_func(router, username, password, sn_number, optical)

