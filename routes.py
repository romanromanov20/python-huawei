#!/usr/bin/python

from app import app, login
import os, ipaddress, json, yaml
#from json2html import *
from flask import Flask, request, jsonify, render_template, redirect, flash, url_for
from olt_autofind import *
from oltread import *
from oltadd import *
from sqlalchemy import text
from oltdelete import *
from oltreset import *
from forms import *
from flask_login import login_required, LoginManager, current_user, login_user, logout_user
from models import *
from werkzeug.security import generate_password_hash, check_password_hash
from acs.acsdelete import *
from acs.acsexists import *
from acs.acsupgrade import *
from acs.acscreate import *


def validateIP(ip):
        try:
                ipaddress.ip_address(ip)
        except ValueError:
                return False
        else:
                return True


def findstotid(json_object, name):
        return [obj for obj in json_object if obj['ont_sn']==name][0]['slot_id']

def findportid(json_object, name):
        return [obj for obj in json_object if obj['ont_sn']==name][0]['port_id']

def findvers(json_object, name):
        return [obj for obj in json_object if obj['ont_sn']==name][0]['version']




@login.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))



@app.route('/login', methods=['GET', 'POST'])
def login():
    if not User.query.filter(User.username == 'admin').first():
        user = User(
            username='admin',
            email='admin@example.com'
        )
        user.set_password('admin1')
        db.session.add(user)
        db.session.commit()

    if current_user.is_authenticated:
        print('Came to this point')
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        return redirect('/')
    return render_template('login.html', title='Sign In', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))



@app.route('/register', methods=['GET', 'POST'])
@login_required
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)



@app.route('/userlist')
@login_required
def userlist():
    users = User.query.order_by(User.id).all()
    return render_template('users.html', users=users )

@app.route('/userupdate/<user>', methods=['GET', 'POST'])
@login_required
def userupdate(user):
    user=int(user)
    usr=User.query.filter_by(id=user).first()

    if request.method == 'POST':
        usr.email = request.form['email']
        passwd = str(request.form['passwd'])
        passwd2 = generate_password_hash(passwd)
        usr.password_hash = passwd2
        
        try:
            db.session.commit()
            user = str(user)
            return redirect('/userupdate/'+user)
        except Exception as e:
            return (str(e))
            

    else:
        
        return render_template('edituser.html', user=usr)



@app.route('/deleteuser/', methods=['GET'])
@login_required
def deleteuser():
    query_parameters = request.args
    id = query_parameters.get('id')
    print('User id is =' +id)
    usrname = User.query.filter_by(id=id).first()
    usr = User.query.get_or_404(id)
    if usrname.username == 'admin':
       flash('Cannot delete admin user!')
       return redirect('/userlist')
    else:
        try:
            db.session.delete(usr)
            db.session.commit()
            flash('User deleted')
            return redirect('/userlist')
        except Exception as e:
            return (str(e))



@app.route('/', methods=['GET', 'POST'])
@login_required
def index():
    page = request.args.get('page', 1, type=int)
    if request.method == 'POST':
        tr069_id = None
        pppuser = None
        ppppass = None
        tlf = None
        tlfpass = None
        sip_id = None
        acs_id = None
        cname = str(request.form.get('cname'))
        serial = str(request.form.get('serial'))
        model = str(request.form.get('model'))
        slotid=None
        portid=None
        activest=None
        name = request.form.get('name')

        #print("requested data"+name,serial,model,slotid,portid, name)
        if ("4857544" not in serial) or (len(serial) != 16) :
            flash('Your serial number is wrong')
            return redirect(url_for('index'))
        if Ont.query.filter_by(serial=serial).first():                             
            flash('This serial number is already registered')
            return redirect(url_for('index'))
        olt_idraw=OLT.query.filter_by(name=name).first()
        olt_id2 = int(olt_idraw.id)
        ssid = str(cname)+'-wifi'
        ssid5g = ssid+'-5G'
        wifipass='ChangeMe'
        try:
            ont=Ont(
                name=cname,
                serial=serial,
                model=model,
                slotid=slotid,
                portid=portid,
                olt_id=olt_id2,
                activest=activest,
                tr069_id = tr069_id,
                pppuser = pppuser,
                ppppass = ppppass,
                tlf = tlf,
                tlfpass = tlfpass,
                sip_id = sip_id,
                acs_id = acs_id,
                ssid=ssid,
                ssid5g=ssid5g,
                wifipass=wifipass,
            )

            db.session.add(ont)
            db.session.commit()
            flash('ONT is added with s/n '+serial)
            return redirect(url_for('index'))
        except Exception as e:
            return (str(e))
    else:
        form = SearchForm(request.form)
        names = OLT.query.with_entities(OLT.name)
        onts = Ont.query.order_by(Ont.id.desc()).paginate(page=page, per_page=10)
        return render_template('index.html', onts=onts, names=names, form=form)





@app.route('/search/serial')  #AJAX SEARCH
@login_required
def searchserial():
    onts = Ont.query.order_by(Ont.id).all()
    list_onts = [r.as_dict() for r in onts]
    return jsonify(list_onts)


@app.route('/process', methods=['POST']) #AJAX (NOT AJAX) OUTPUT
def process():
    serial = request.form['serial']
    if serial:
        print(serial)
        form = SearchForm(request.form)
        page = request.args.get('page', 1, type=int)
        names = OLT.query.with_entities(OLT.name)
        search = "%{}%".format(serial)
        onts = Ont.query.filter(Ont.serial.like(search)).paginate(page=page, per_page=10)
        return render_template('index.html', onts=onts, names=names, form=form)
    return jsonify({'error': 'missing data..'})










@app.route('/delete/', methods=['GET'])
@login_required
def delete():
    query_parameters = request.args
    id = query_parameters.get('id')
    print(id)
    ont = Ont.query.get_or_404(id)

    try:
        db.session.delete(ont)
        db.session.commit()
        return redirect('/')
    except Exception as e:
        return (str(e))


@app.route('/olt', methods=['GET', 'POST'])
@login_required
def olts():
    if request.method == 'POST':
        ip = ""
        cname = str(request.form.get('name'))
        if not cname:
           flash('Name must be not empty')
           return redirect(url_for('olts'))
        if Services.query.filter_by(name=cname).first():
            flash('This Name is already choosen')
            return redirect(url_for('olts'))
        rawip = str(request.form.get('ip'))
        username = str(request.form.get('username'))
        password = request.form.get('password')
        tr069 = request.form.get('tr069')
        linepr = request.form.get('linepr')
        service = request.form.get('servv')
        

        name=service.replace("(", "").replace(")", "").replace("'", "").replace(",", "")  #FIND BETTER WAY!
        serv_idraw=Services.query.filter_by(name=name).first()
        serv_id2 = int(serv_idraw.id)

        if validateIP(rawip):
           ip = rawip
        else:
           flash('This IPv4 is wrong')
           return redirect(url_for('olts'))
        try:
            olt=OLT(
                name=cname,
                ip=ip,
                username=username,
                password=password,
                tr069=tr069,
                linepr=linepr,
                service_id=serv_id2,
            )
            
            db.session.add(olt)
            db.session.commit()
            flash('OLT is added with s/n '+ip)
            return redirect(url_for('olts'))
        except Exception as e:
            return (str(e))
    else:
        olts = OLT.query.order_by(OLT.id).all()
        services = Services.query.order_by(Services.name).all()
        print(services)
        return render_template('olts.html', olts=olts, services=services)


@app.route('/deleteolt/<int:id>')
@login_required
def deleteolt(id):
    olt = OLT.query.get_or_404(id)

    try:
        db.session.delete(olt)
        db.session.commit()
        return redirect('/olt')
    except Exception as e:
        return (str(e))

@app.route('/oltautofind/', methods=['GET'])
@login_required
def oltautofind():
    query_parameters = request.args
    router = query_parameters.get('ip')
    username = query_parameters.get('username')
    password = query_parameters.get('password')
    results = autofind_func(router, username, password)
    olts = OLT.query.order_by(OLT.id).all()
    return render_template('olts.html', olts=olts, results=results)



@app.route('/getontinfo/', methods=['GET'])
@login_required
def api_id():
    page = request.args.get('page', 1, type=int)
    query_parameters = request.args
    sn_number = query_parameters.get('sn')
    ont_row=Ont.query.filter_by(serial=sn_number).first()
    olt_id=ont_row.olt_id
    olt_idraw=OLT.query.filter_by(id=olt_id).first()
    router = olt_idraw.ip
    username = olt_idraw.username
    password = olt_idraw.password
    slotid = None
    portid = None
    verss = 'unknown '
    results = service_func(router, username, password, sn_number)
    results0 = results[0]
    results2 = results[1]
    results3 = results[2]
    try:
       results4 = results[3]
       results5 = results[4]
       results6 = results[5]    
    except:
        results4 = ''
        results5 = ''
        results6 = ''

    if results0 == "The required ONT does not exist":
        activest = "not added"
        results = autofind_func(router, username, password)
        print('Results Autofind '+results)
        results = json.loads(results)
        fsp = results['onts']
        fspline = str(fsp)
        if 'No_ONTs_to_add' in fspline:
            flash('This ONT cannot be found')
        else:
            try:
                slotid = findstotid(fsp, sn_number)
                portid = findportid(fsp, sn_number)
                verss = findvers(fsp, sn_number) 
            except:
                flash('This ONT cannot be RECOGNISED')    
    else:
        activest = "added"
        result3 = results[3]
        result0 = json.loads(results0)
        fsp = result0['maininfo']
        stats = fsp[0]["Run state"]
        if stats == "online":
            fsp = fsp[0]["F/S/P"]
            slotid = fsp.split('/')[1]
            portid = fsp.split('/')[2]
            result3 = json.loads(result3)
            vers = result3['version']
            verss = vers[0]["Equipment-ID"]
    
    try:
        updatedb = Ont.query.filter_by(serial=sn_number).first()
        updatedb.slotid = slotid
        updatedb.portid = portid
        updatedb.activest = activest
        updatedb.model = verss
        db.session.commit()
    except Exception as e:
        return (str(e))


    
    try:
        results1=json.loads(results0)['maininfo'][0]
        results2=json.loads(results2)['services']
        results3=json.loads(results3)['wan-info']
        results4=json.loads(results4)['version'][0]
        results5=json.loads(results5)['register-info']
        results6=json.loads(results6)['optical-info'][0]
    except:
        results1=''
        results2=''
        results3=''
        results4=''
        results5=''
        results6=''

    names = OLT.query.with_entities(OLT.name)    
    form = SearchForm(request.form)
    onts = Ont.query.order_by(Ont.id.desc()).paginate(page=page, per_page=10)
    
    return render_template('index.html', onts=onts, names=names, results1=results1, results2=results2, results3=results3, results4=results4, results5=results5, results6=results6, form=form)


@app.route('/ontadd/', methods=['GET'])
@login_required
def ont_add():
    


    query_parameters = request.args
    page = request.args.get('page', 1, type=int)
    serialnumber = query_parameters.get('sn')
    
    ont_row=Ont.query.filter_by(serial=serialnumber).first()
    descr = ont_row.name
    slot_id = ont_row.slotid
    port_id = ont_row.portid
    olt_id = ont_row.olt_id


    olt_raw=OLT.query.filter_by(id=olt_id).first()
    router = olt_raw.ip
    username = olt_raw.username
    password = olt_raw.password
    tr069 = olt_raw.tr069
    linepr = olt_raw.linepr
    service_id=olt_raw.service_id
    
    serv_row = Services.query.filter_by(id=service_id).first()
    svlan1 = serv_row.svlan1
    svlan2 = serv_row.svlan2
    svlan3 = serv_row.svlan3
    cvlan1 = serv_row.cvlan1
    cvlan2 = serv_row.cvlan2
    cvlan3 = serv_row.cvlan3
    gemport1 = serv_row.gemport1
    gemport2 = serv_row.gemport2
    gemport3 = serv_row.gemport3
    ttinternet = serv_row.ttinternet
    ttvoip = serv_row.ttvoip 
    form = SearchForm(request.form)

    results = ontadd(router, username, password, serialnumber, slot_id, port_id, descr, tr069, linepr, svlan1, svlan2, svlan3, cvlan1, cvlan2, cvlan3, gemport1, gemport2, gemport3, ttinternet, ttvoip)
    names = OLT.query.with_entities(OLT.name) 
    onts = Ont.query.order_by(Ont.id.desc()).paginate(page=page, per_page=10)
    flash(results) 
    return render_template('index.html', onts=onts, names=names, form=form)



@app.route('/ontdelete/', methods=['GET'])
@login_required
def ont_delete():
    query_parameters = request.args
    page = request.args.get('page', 1, type=int)
    sn_number = query_parameters.get('sn')
    ont_row=Ont.query.filter_by(serial=sn_number).first()
    olt_id=ont_row.olt_id
    olt_raw=OLT.query.filter_by(id=olt_id).first()
    router = olt_raw.ip
    username = olt_raw.username
    password = olt_raw.password
    
    results = olt_remove(router, username, password, sn_number)
    names = OLT.query.with_entities(OLT.name)  
    form = SearchForm(request.form)   
    onts = Ont.query.order_by(Ont.id.desc()).paginate(page=page, per_page=10) 
    flash(results)
    return render_template('index.html', onts=onts, names=names, form=form)


@app.route('/ontreset/', methods=['GET'])
@login_required
def ont_reset():
    query_parameters = request.args
    page = request.args.get('page', 1, type=int)
    sn_number = query_parameters.get('sn')
    olt_id=ont_row.olt_id
    olt_raw=OLT.query.filter_by(id=olt_id).first()
    router = olt_raw.ip
    username = olt_raw.username
    password = olt_raw.password
    results = ontreset(router, username, password, sn_number)
    names = OLT.query.with_entities(OLT.name) 
    sql = text('select a.id, a.serial, a.model, a.slotid, a.portid, a.activest, a.name, a.olt_id, b.ip from ont_api a, olt_api b where a.olt_id = b.id;')
    onts = db.engine.execute(sql)
    form = SearchForm(request.form)
    flash(results)
    return render_template('index.html', onts=onts, names=names, form=form)


@app.route('/service', methods=['GET', 'POST'])
@login_required
def oltservices():
    if request.method == 'POST':
        name = str(request.form.get('name'))
        svlan1 = str(request.form.get('svlan1'))
        svlan2 = str(request.form.get('svlan2'))
        svlan3 = str(request.form.get('svlan3'))
        cvlan1 = str(request.form.get('cvlan1'))
        cvlan2 = str(request.form.get('cvlan2'))
        cvlan3 = str(request.form.get('cvlan3'))
        gemport1 = str(request.form.get('gemport1'))
        gemport2 = str(request.form.get('gemport2'))
        gemport3 = str(request.form.get('gemport3'))
        ttinternet = str(request.form.get('ttinternet'))
        ttvoip = str(request.form.get('ttvoip'))

        try:
            services=Services(
                name=name,
                svlan1=svlan1,
                svlan2=svlan2,
                svlan3=svlan3,
                cvlan1=cvlan1,
                cvlan2=cvlan2,
                cvlan3=cvlan3,
                gemport1=gemport1,
                gemport2=gemport2,
                gemport3=gemport3,
                ttinternet=ttinternet,
                ttvoip=ttvoip
            )
            
            db.session.add(services)
            db.session.commit()
            flash('Service is added ' + name)
            return redirect(url_for('oltservices'))
        except Exception as e:
            return (str(e))
    else:
        services = Services.query.order_by(Services.id).all()
        return render_template('service.html', services=services)



@app.route('/deleteservice/<int:id>')
@login_required
def deleteservice(id):
    service = Services.query.get_or_404(id)

    try:
        db.session.delete(service)
        db.session.commit()
        return redirect('/service')
    except Exception as e:
        return (str(e))





@app.route('/<serial>')
@login_required
def serialinfo(serial):
    print('Serial is'+serial)
    try:
        serial=str(serial)
        acs=None
        resp1 = 2
        ont=Ont.query.filter_by(serial=serial).first()
        olt_id=ont.olt_id
        model=ont.model
        olt=OLT.query.filter_by(id=olt_id).first()
        sip_id=ont.sip_id
        sip=SIP.query.filter_by(id=sip_id).first()
        tr069_id=ont.tr069_id
        tr069profile=TR069Profile.query.filter_by(id=tr069_id).first()
        if tr069profile:
            model=ont.model.rstrip()
            acsid=tr069profile.acs_id
            acs=ACS.query.filter_by(id=acsid).first()
            acsurl = acs.ip
            apiport = '7557'

            if model == '245H':
                model = 'HG8245H'
            resp = acsexist(acsurl, apiport, model, serial)
            
            if str(resp) == '[]':
                resp1 = 2
            else:
                resp1 = 1

        
        return render_template('onts.html', ont=ont, olt=olt, sip=sip, tr069profile=tr069profile, acs=acs, model=model, resp1=resp1)
    except Exception as e:
        return (str(e))
#        flash('Serial is not found')
#        return redirect(url_for('index'))


@app.route('/serialupdate/<serial>', methods=['GET', 'POST'])
@login_required
def serialupdate(serial):
    serial=str(serial)
    ont=Ont.query.filter_by(serial=serial).first()
    olt_id=ont.olt_id
    olt=OLT.query.filter_by(id=olt_id).first()
    sip_id=ont.sip_id
    sip = SIP.query.filter_by(id=sip_id).first()
    tr069_id = ont.tr069_id
    tr069 = TR069Profile.query.filter_by(id=tr069_id).first()



    sipp = SIP.query.order_by(SIP.id).all()
    tr069_ = TR069Profile.query.order_by(TR069Profile.id).all()

    if request.method == 'POST':
        ont.name = request.form['name']
        ont.model = request.form['model']
        ont.pppuser = request.form['pppuser']
        ont.ppppass = request.form['ppppass']
        sipname = request.form['sipname']
        ssid = request.form['ssid']
        ssid5g = request.form['ssid5g']
        wifipass = request.form['wifipass']
        sip = SIP.query.filter_by(name=sipname).first()
        ont.sip_id = sip.id
        tr069name = request.form['tr069name']
        tr069 = TR069Profile.query.filter_by(name=tr069name).first()
        ont.tr069_id = tr069.id
        try:
            db.session.commit()
            return redirect('/'+serial)
        except:
            return "There was a problem updating data."
        
    else:
        print (sipp)
        print (tr069_)
        return render_template('serialupdate.html', ont=ont, olt=olt, sip=sip, tr069=tr069, sipp=sipp, tr069_=tr069_)



@app.route('/acsdelete/<int:id>')
@login_required
def acsdelete(id):
    ont = Ont.query.get_or_404(id)
    olt_id=ont.olt_id
    olt=OLT.query.filter_by(id=olt_id).first()
    sip_id=ont.sip_id
    sip=SIP.query.filter_by(id=sip_id).first()
    tr069_id=ont.tr069_id
    tr069profile=TR069Profile.query.filter_by(id=tr069_id).first()
    acsid=tr069profile.acs_id
    acs=ACS.query.filter_by(id=acsid).first()
    acsurl = acs.ip
    apiport = '7557'
    modl = ont.model.rstrip()
    model = modl
    if modl == '245H':
        model = 'HG8245H'
    serial = ont.serial
    resp = acspresetdelete(acsurl, apiport, model, serial)
    #resp2 = acsdevicedelete(acsurl, apiport, model, serial)
    if resp == '<Response [200]>':
        #if resp2 == '<Response [200]>':
            ont.acs_id = None
            acs = None
            flash('ACS preset and device are removed')
            return redirect(url_for('serialinfo', serial=serial))
    else:
        flash('could not remove properly')
        return redirect(url_for('serialinfo', serial=serial))



@app.route('/acsupgrade/<int:id>')
@login_required
def acsupgrade(id):
    ont = Ont.query.get_or_404(id)
    olt_id=ont.olt_id
    olt=OLT.query.filter_by(id=olt_id).first()
    sip_id=ont.sip_id
    sip=SIP.query.filter_by(id=sip_id).first()
    profile_id=ont.tr069_id
    tr069profile=TR069Profile.query.filter_by(id=profile_id).first()
    internet=tr069profile.internet
    voip=tr069profile.voip
    acsid=tr069profile.acs_id
    acs=ACS.query.filter_by(id=acsid).first()
    acsurl = acs.ip
    apiport = '7557'
    modl = ont.model.rstrip()
    model = modl
    if modl == '245H':
        model = 'HG8245H'
    serial = ont.serial
    ssid = ont.ssid
    ssid5g = ont.ssid5g
    wifipass = ont.wifipass
    pppuser = ont.pppuser
    ppppass = ont.ppppass
    tlf = ont.tlf
    tlfpass = ont.tlfpass 
    resp1 = 2

    resp = acsexist(acsurl, apiport, model, serial)
    if str(resp) == '[]':
        resp1 = 2
    else:
        resp1 = 1
    result1 = acssend(acsurl, apiport, model, serial, internet, voip)
    print(result1)

    if str(result1[0]) == '<Response [200]>':

        result = acspresetupgrade(acsurl, apiport, model, serial, ssid, ssid5g, wifipass, pppuser, ppppass, tlf, tlfpass, profile_id )
        print(result)
        flash(result)
    elif str(result1[0]) == '<Response [202]>':
        flash('ONT is offline')
    else:
        flash('Some problems updating ACS profile')
    return render_template('onts.html', ont=ont, olt=olt, sip=sip, tr069profile=tr069profile, acs=acs, model=modl, resp1=resp1)








@app.route('/acs', methods=['GET', 'POST'])
@login_required
def acs():
    if not ACS.query.filter(ACS.name == 'Default').first():
        acs = ACS(
            name='Default',
            ip='genieacs',
            username='admin',
            password='admin',
            port='7557',
        )
        db.session.add(acs)
        db.session.commit()
    if request.method == 'POST':
        ip = ""
        name = str(request.form.get('name'))
        if not name:
           flash('Name must be not empty')
           return redirect(url_for('acs'))
        if ACS.query.filter_by(name=name).first():
            flash('This Name is already choosen')
            return redirect(url_for('acs'))
        ip = str(request.form.get('ip'))
        username = str(request.form.get('username'))
        password = request.form.get('password')
        port = request.form.get('port')
        
        try:
            acs=ACS(
                name=name,
                ip=ip,
                username=username,
                password=password,
                port=port,
            )
            
            db.session.add(acs)
            db.session.commit()
            flash('ACS is added with ip '+ip)
            return redirect(url_for('acs'))
        except Exception as e:
            return (str(e))
    else:
        acss = ACS.query.order_by(ACS.id).all()
        return render_template('acs.html', acss=acss)


@app.route('/acsupdate/<id>', methods=['GET', 'POST'])
@login_required
def acsupdate(id):
    acs = ACS.query.get_or_404(id)
    if request.method == 'POST':
        name = str(request.form.get('name'))
        if not name:
           flash('Name must be not empty')
           return redirect(url_for('acs'))
        else:
            acs.name=name
        acs.ip = str(request.form.get('ip'))
        acs.username = str(request.form.get('username'))
        acs.password = request.form.get('password')
        acs.port = request.form.get('port')
        
        try:
            db.session.commit()
            flash('ACS is updated')
            return redirect('/acsupdate/'+id)
        except Exception as e:
            return (str(e))
    else:
        return render_template('acsupdate.html', acs=acs)



@app.route('/deleteacs/<int:id>')
@login_required
def deleteacs(id):
    acs = ACS.query.get_or_404(id)

    try:
        db.session.delete(acs)
        db.session.commit()
        return redirect('/acs')
    except Exception as e:
        return (str(e))








@app.route('/sip', methods=['GET', 'POST'])
@login_required
def sip():
    if not SIP.query.filter(SIP.name == 'Default').first():
        sip = SIP(
            name='Default',
            ip='freepbx',
            port='5060',
        )
        db.session.add(sip)
        db.session.commit()
    if request.method == 'POST':
        ip = ""
        name = str(request.form.get('name'))
        if not name:
           flash('Name must be not empty')
           return redirect(url_for('sip'))
        if SIP.query.filter_by(name=name).first():
            flash('This Name is already choosen')
            return redirect(url_for('sip'))
        ip = str(request.form.get('ip'))
        port = request.form.get('port')
        
        try:
            sip=SIP(
                name=name,
                ip=ip,
                port=port,
            )
            db.session.add(sip)
            db.session.commit()
            flash('SIP is added with ip '+ip)
            return redirect(url_for('sip'))
        except Exception as e:
            return (str(e))
    else:
        sips = SIP.query.order_by(SIP.id).all()
        return render_template('sip.html', sips=sips)

@app.route('/deletesip/<int:id>')
@login_required
def deletesip(id):
    sip = SIP.query.get_or_404(id)

    try:
        db.session.delete(sip)
        db.session.commit()
        return redirect('/sip')
    except Exception as e:
        return (str(e))







@app.route('/tr069campos', methods=['GET', 'POST'])
@login_required
def tr069campos():
    if request.method == 'POST':
        name = str(request.form.get('name'))
        if not name:
           flash('Name must be not empty')
           return redirect(url_for('tr069campos'))
        if SIP.query.filter_by(name=name).first():
            flash('This Name is already choosen')
            return redirect(url_for('tr069campos'))
        value = str(request.form.get('value'))
        try:
            campos=TR069Campos(
                name=name,
                value=value,
            )
            db.session.add(campos)
            db.session.commit()
            flash('TR069 line is added with')
            return redirect(url_for('tr069campos'))
        except Exception as e:
            return (str(e))
    else:
        campos = TR069Campos.query.order_by(TR069Campos.id).all()
        return render_template('tr069campos.html', campos=campos)

@app.route('/deletecampo/<int:id>')
@login_required
def deletecampo(id):
    campo = TR069Campos.query.get_or_404(id)

    try:
        db.session.delete(campo)
        db.session.commit()
        return redirect('/tr069campos')
    except Exception as e:
        return (str(e))






@app.route('/tr069profiles', methods=['GET', 'POST'])
@login_required
def tr069profiles():
    if request.method == 'POST':
        name = str(request.form.get('name'))
        if not name:
           flash('Name must be not empty')
           return redirect(url_for('tr069campos'))
        if SIP.query.filter_by(name=name).first():
            flash('This Name is already choosen')
            return redirect(url_for('tr069campos'))
        internet = str(request.form.get('internet'))
        voip = str(request.form.get('voip'))
        acsname = str(request.form.get('acsname'))
        acsall = ACS.query.filter_by(name=acsname).first()
        acs_id = acsall.id

        try:
            profile=TR069Profile(
                name=name,
                internet=internet,
                voip=voip,
                acs_id=acs_id,
            )
            db.session.add(profile)
            db.session.commit()
            flash('TR069 profile is added with')
            return redirect(url_for('tr069profiles'))
        except Exception as e:
            return (str(e))
    else:
        acss = ACS.query.order_by(ACS.id).all()
        profiles = TR069Profile.query.order_by(TR069Profile.id).all()
        return render_template('tr069profiles.html', profiles=profiles, acss=acss)

@app.route('/deleteprofile/<int:id>')
@login_required
def deleteprofile(id):
    profile = TR069Profile.query.get_or_404(id)

    try:
        db.session.delete(profile)
        db.session.commit()
        return redirect('/tr069profiles')
    except Exception as e:
        return (str(e))




@app.route('/profile/<profileid>', methods=['GET', 'POST'])
@login_required
def tr069paramprofile(profileid):
    print('Profileid is'+profileid)
    try:
        profileid=str(profileid)
        if request.method == 'POST':
            value = str(request.form.get('value'))
            campo_name = str(request.form.get('campo_name'))
            
            campo = TR069Campos.query.filter_by(value=campo_name).first()
            campo_id = campo.id

            try:
                profile=TR069ParametersProfile(
                    profile_id=profileid,
                    campo_id=campo_id,
                    value=value,
                    
                )
                db.session.add(profile)
                db.session.commit()
                flash('TR069 Parameters are added')
                return redirect('/profile/'+profileid)
            except Exception as e:
                return (str(e))
        else:
            profileid = int(profileid)
            campos = TR069Campos.query.order_by(TR069Campos.id).all()
            sql = text('select p.id as mainid, c.value as cvalue, p.value as pvalue, p.profile_id as profid from tr069_api c, tr069_parameters p where p.campo_id = c.id;')
            profiles = db.engine.execute(sql)
            return render_template('tr069paramprofiles.html', profiles=profiles, campos=campos, profileid=profileid)
        
        
    except Exception as e:
        return (str(e))


@app.route('/deleteparamprofile/<id>', methods=['GET', 'POST'])
@login_required
def tr069paramprofileentry(id):
    lines=TR069ParametersProfile.query.get_or_404(id)
    profile_id = str(lines.profile_id)
    try:
        db.session.delete(lines)
        db.session.commit()
        return redirect('/profile/'+profile_id)
    except Exception as e:
        return (str(e))






app.run(host= '0.0.0.0', port=5000)
