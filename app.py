#!/usr/bin/python
from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
import logging
from logging.handlers import RotatingFileHandler


app = Flask(__name__)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)


app.config['ENV'] = 'development'
app.config['DEBUG'] = True
app.config['TESTING'] = True


login = LoginManager(app)
login.login_view = 'login'

import routes
import models

if __name__ == '__main__':
    handler = RotatingFileHandler('foo.log', maxBytes=10000, backupCount=1)
    handler.setLevel(logging.INFO)
    app.logger.addHandler(handler)
    app.run(debug=True)