#!/usr/bin/env python
import paramiko, time, sys

def ontadd(router, username, password, serialnumber, slot_id, port_id, descr, tr069, linepr, svlan1, svlan2, svlan3, cvlan1, cvlan2, cvlan3, gemport1, gemport2, gemport3, ttinternet, ttvoip):
	n=0
	ssh = paramiko.SSHClient()
	ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	ssh.connect(router, username=username, password=password,allow_agent=False, look_for_keys=False)
	remote_conn = ssh.invoke_shell()
	output = remote_conn.recv(1000)


	remote_conn.send('enable\n')
	remote_conn.send('config\n')
	remote_conn.send('scroll 512\n')
	remote_conn.send('display ont info by-sn '+ serialnumber + ' \n')
	remote_conn.send(' \n')
	time.sleep(2)
	outpt = remote_conn.recv(9999)
	output = outpt.decode()

	for line in output.split('\n'):
		if "The required ONT does not exist" in line:
			maininfo = 'The required ONT does not exist'
			n=1
	if n==0:
			return('{"ONT is already created"}')
			sys.exit()



	remote_conn.send('display ont info 0 ' + slot_id + ' ' + port_id + ' all\n')
	remote_conn.send(' \n')
	time.sleep(3)
	outpt3 = remote_conn.recv(9999)
	output3 = outpt3.decode()
	
	searchline = str('0/ '+slot_id+'/'+port_id+'  ')
	line_id = -1
	for line in output3.split('\n'):
		if searchline in line:
			str_onuid = line.split()
			line_id = str_onuid[2]

	onu_id_free=int(line_id)+1
	
	
	remote_conn.send('interface gpon 0/' + slot_id + ' \n')
	time.sleep(1)

	print('ont add ' + str(port_id) + ' ' + str(onu_id_free) + ' sn-auth ' + str(serialnumber) + ' omci ont-lineprofile-id '+str(linepr)+' ont-srvprofile-id 1 desc ' + str(descr) )
	remote_conn.send('ont add ' + str(port_id) + ' ' + str(onu_id_free) + ' sn-auth ' + str(serialnumber) + ' omci ont-lineprofile-id '+str(linepr)+' ont-srvprofile-id 1 desc ' + str(descr)+' \n' )
	remote_conn.send(' \n')
	if tr069:
		time.sleep(1)
		print('ont ipconfig ' + str(port_id) + ' ' + str(onu_id_free) + ' dhcp vlan '+ cvlan1 +' priority 5')
		remote_conn.send('ont ipconfig ' + str(port_id) + ' ' + str(onu_id_free) + ' dhcp vlan '+ cvlan1 +' priority 5  \n')
		remote_conn.send(' \n')


		time.sleep(1)
		print('ont tr069-server-config ' + str(port_id) + ' ' + str(onu_id_free) + ' profile-id '+ tr069)
		remote_conn.send('ont tr069-server-config ' + str(port_id) + ' ' + str(onu_id_free) + ' profile-id '+ tr069+' \n')
		remote_conn.send(' \n')


	time.sleep(1)
	remote_conn.send('quit \n')

	time.sleep(1)
	command1 = 'service-port vlan '+ str(svlan1) +' gpon 0/'+ str(slot_id) +'/'+ str(port_id) + ' ont ' + str(onu_id_free) +' gemport '+ str(gemport1) +' multi-service user-vlan '+ str(cvlan1) +' tag-transform translate \n'
	print(command1)
	remote_conn.send(command1) #create service-port for tr-069
	remote_conn.send(' \n')
	remote_conn.send(' \n')
	remote_conn.send(' \n')
	time.sleep(2)
	if svlan2:
		command2 = 'service-port vlan '+ str(svlan2) +' gpon 0/'+ str(slot_id) +'/'+ str(port_id) +' ont '+ str(onu_id_free) +' gemport '+ str(gemport2) +' multi-service user-vlan '+str(cvlan2)+' tag-transform translate inbound traffic-table index '+str(ttinternet)+' outbound traffic-table index '+str(ttinternet)+' \n'
		print(command2)
		remote_conn.send(command2)
		remote_conn.send(' \n')
		remote_conn.send(' \n')
		time.sleep(2)
		if svlan3:
			command3 = 'service-port vlan '+str(svlan3)+' gpon 0/'+ str(slot_id) +'/'+ str(port_id) +' ont '+ str(onu_id_free) +' gemport '+str(gemport3)+' multi-service user-vlan '+str(cvlan3)+' tag-transform translate inbound traffic-table index '+str(ttvoip)+' outbound traffic-table index '+str(ttvoip)+' \n'
			print(command3)
			remote_conn.send(command3) # create voip service with corresponding traffic table
			remote_conn.send(' \n')
			time.sleep(2)

	
	time.sleep(2)
	outpt2 = remote_conn.recv(9999)
	output2 = outpt2.decode()
	print(output2)
	return '{"ONT added": "'+serialnumber+'" }'
	
	ssh.close() 


if __name__ == '__main__':
	ontadd(router, username, password, serialnumber, slot_id, port_id, descr)

