FROM alpine

LABEL MAINTANER Roman Romanov roman_romanovv@mail.ru

#ENV GROUP_ID=1000 \
#    USER_ID=1000

RUN apk update && apk add --no-cache alpine-sdk libressl-dev musl-dev libffi-dev && \
    apk add --no-cache openssl-dev libffi-dev gcc musl-dev make postgresql-client postgresql-dev && apk add --virtual build-deps && \
    apk add --no-cache python3 python3-dev && \
    if [ ! -e /usr/bin/python ]; then ln -sf python3 /usr/bin/python ; fi && \
    \
    echo "**** install pip ****" && \
    python3 -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip3 install --no-cache --upgrade pip setuptools wheel && \
    if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi


COPY ./requirements.txt /app/requirements.txt

WORKDIR /app

COPY . /app

RUN chmod +x startscript.sh && pip3 install psycopg2 requests pytest && pip install -r requirements.txt
#RUN pip install gunicorn

#RUN addgroup -g $GROUP_ID www
#RUN adduser -D -u $USER_ID -G www www -s /bin/sh

#USER www

#EXPOSE 5000

ENTRYPOINT [ "python" ]

CMD [ "app.py" ]






