#!/usr/bin/env python
import paramiko, time, sys


def ontreset(router, username, password, sn_number):

	ssh = paramiko.SSHClient()
	ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	ssh.connect(router, username=username, password=password,allow_agent=False, look_for_keys=False)
	remote_conn = ssh.invoke_shell()
	output = remote_conn.recv(1000)
	remote_conn.send('enable\n')
	remote_conn.send('scroll 512\n')
	remote_conn.send('display ont info by-sn '+ sn_number + ' \n')
	remote_conn.send(' \n')
	time.sleep(2)
	outpt = remote_conn.recv(9999)
	output = outpt.decode()

	for line in output.split('\n'):
		if "The required ONT does not exist" in line:
			print("The required ONT does not exist!")
			results = '{"ONT does not exist" : "'+sn_number+'" }'
			return results
			sys.exit()
		if "ONT-ID" in line:
			str_onuid = line.split(':')
			line_id = str_onuid[1].lstrip().rstrip('\r')
		if "F/S/P" in line:
			str_onuid = line.split(':')
			line2_id = str_onuid[1].lstrip().rstrip('\r')
			slot_id = line2_id.split('/')[1]
			port_id = line2_id.split('/')[2]
	try:
		print('reset slot id ='+ slot_id + ' port id = ' + port_id + ' ONT ID = '+ line_id)
#		remote_conn.send('config \n')
#		remote_conn.send('interface gpon 0/'+ str(slot_id) +' \n') 
#		remote_conn.send('ont factory-setting-restore '+ str(port_id) +' '+ str(line_id) +'  \n')
#		remote_conn.send(' \n')
#		remote_conn.send(' \n')
#		time.sleep(2)
#		output = remote_conn.recv(9999)
#		output = outpt.decode()
		results = '{"ONT resetted" : "'+sn_number+'" }'
		return results
	except Exception as e:
		print(e)


	ssh.close()

if __name__ == '__main__':
	api_func(host= '0.0.0.0')
