# Python Flask basic provisioning WEB server.

Requirements:

Tested on Docker version 19.03.8, docker-compose version 1.25.4


## Installation:

Download repo by executing

> git clone https://gitlab.com/romanromanov20/python-huawei.git

Start docker-compose file

> docker-compose up -d

For the initial setup you need to create a database by executing from your host machine:

> docker exec -it flask-container sh /app/startscript.sh

Default username: admin
Default password: admin1

You can create and delete other usernames. But you cannot delete the admin username


## Usage with web interface:

`http://host-ip:80` 

GenieACS is reachable on 

`http://host-ip:3000`

Port is configurable in docker-compose file

Allows to create, edit, delete, reset, and get info about ONTs.

Allows to configure multiple OLTs, multiple TR-069, multiple Vlan/Gem/Traffic-table configs.

Allows to create different GenieACS profiles for dynamic provisioning (app engine is under development still)

Missing: Elasticsearch, pagination

Compatible for a NEBA usage. 

Allows to create up to 3 cvlans per ONT.




## Usage for pure REST API:

1) To Autofind use: 

> curl -X GET -G 'http://127.0.0.1:80/oltautofind' -d olt=10.1.0.1 -d username=admin -d password=notasecret

Returns all ONTS taht are not registered in OLT. Returns s/n, Frame, Slot and Port. Possible to exten to return an ONT version. 

2) To get info from ONT use: 

> curl -X GET -G 'http://127.0.0.1:80/getontinfo' -d sn=48575443B48D6856  -d olt=10.1.0.1 -d username=admin -d password=notasecret

Returns info about ONT status (online/offline, F/S/P and ONT ID), it's service-ports and info about wan-interfaces. Possible to extend to cover info about ETH ports, POTS ports, WLANs.

3) To add ONT: 

> curl -X GET -G 'http://127.0.0.1:80/ontadd' -d sn=48575443B48D6856 -d slot=1 -d port=1 -d descr=MyCustomer1 -d olt=10.1.0.1 -d username=admin -d password=notasecret -d cvlan1=1001 -d svlan=9 -d linepr=1 -d gemport1=11

(Need to add parameters like cvlan1, svlan1, gemport1, linepr, tr069, etc.)


4) To delete ONT: 

> curl -X GET -G 'http://127.0.0.1:80/ontdelete' -d sn=48575443B48D6856 -d olt=10.1.0.1 -d username=admin -d password=notasecret

Removes ONT from OLT.

5) To reset ONT:

> curl -X GET -G 'http://127.0.0.1:80/ontreset' -d sn=48575443B48D6856 -d olt=10.1.0.1 -d username=admin -d password=notasecret

Replace 127.0.0.1 with your current server IP.
