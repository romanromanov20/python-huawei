#!/usr/bin/python

from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from app import db
#from sqlalchemy import Table, Column, Integer, ForeignKey
#from sqlalchemy.orm import relationship
#from sqlalchemy.ext.declarative import declarative_base





class Ont(db.Model):
    __tablename__ = 'ont_api'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    serial = db.Column(db.String())
    model = db.Column(db.String())
    slotid = db.Column(db.String())
    portid = db.Column(db.String())
    olt_id = db.Column(db.Integer, db.ForeignKey('olt_api.id'))
    activest = db.Column(db.String())
    tr069_id = db.Column(db.Integer, db.ForeignKey('tr069_profile.id'))
    pppuser = db.Column(db.String())
    ppppass = db.Column(db.String())
    tlf = db.Column(db.String())
    tlfpass = db.Column(db.String())
    sip_id = db.Column(db.Integer, db.ForeignKey('sip_api.id'))
    acs_id = db.Column(db.Integer, db.ForeignKey('acs_api.id'))
    ssid = db.Column(db.String())
    ssid5g = db.Column(db.String())
    wifipass = db.Column(db.String())

    def __init__(self, name, serial, model, slotid, portid, olt_id, activest, tr069_id, pppuser, ppppass, tlf, tlfpass, sip_id, acs_id, ssid, ssid5g, wifipass):
        self.name = name
        self.serial = serial
        self.model = model
        self.slotid = slotid
        self.portid = portid
        self.olt_id = olt_id
        self.activest = activest
        self.tr069 = tr069_id
        self.pppuser = pppuser
        self.ppppass = ppppass
        self.tlf = tlf
        self.tlfpass = tlfpass
        self.sip = sip_id
        self.acs = acs_id
        self.ssid = ssid
        self.ssid5g = ssid5g
        self.wifipass = wifipass

    def __repr__(self):
        return '<id {}>'.format(self.id)

    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'serial': self.serial,
            'model': self.model,
            'slot':self.slotid,
            'port':self.portid,
            'olt_id':self.olt_id,
            'activest':self.activest,
            'tr069_id': self.tr069_id,
            'pppuser': self.pppuser,
            'ppppass':self.ppppass,
            'tlf':self.tlf,
            'tlfpass':self.tlfpass,
            'sip_id':self.sip_id,
            'acs_id':self.acs_id,
            'ssid':self.ssid,
            'ssid5g':self.ssid5g,
            'wifipass':self.wifipass,
        }

    def as_dict(self):
        return {'serial': self.serial}

class OLT(db.Model):
    __tablename__ = 'olt_api'

    id = db.Column(db.Integer, primary_key=True)
    ip = db.Column(db.String())
    username = db.Column(db.String())
    password = db.Column(db.String())
    tr069 = db.Column(db.String())
    linepr = db.Column(db.String())
    name = db.Column(db.String())
    service_id = db.Column(db.Integer, db.ForeignKey('services_api.id'))
#    onts = relationship("Onts")

    def __init__(self, name, ip, username, password, tr069, linepr, service_id):
        self.ip = ip
        self.username = username
        self.password = password
        self.tr069 = tr069
        self.linepr = linepr
        self.name = name
        self.service_id = service_id 
    def __repr__(self):
        return '<id {}>'.format(self.id)

    def serialize(self):
        return {
            'id': self.id,
            'ip': self.ip,
            'username': self.username,
            'password':self.password,
            'tr069':self.tr069,
            'linepr':self.linepr,
            'name':self.name,
            'service_id':self.service_id
        }

class Services(db.Model):
    __tablename__ = 'services_api'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    svlan1 = db.Column(db.String())
    svlan2 = db.Column(db.String())
    svlan3 = db.Column(db.String())
    cvlan1 = db.Column(db.String())
    cvlan2 = db.Column(db.String())
    cvlan3 = db.Column(db.String())
    gemport1 = db.Column(db.String())
    gemport2 = db.Column(db.String())
    gemport3 = db.Column(db.String())
    ttinternet = db.Column(db.String())
    ttvoip = db.Column(db.String())

    def __init__(self, name, svlan1, svlan2, svlan3, cvlan1, cvlan2, cvlan3, gemport1, gemport2, gemport3, ttinternet, ttvoip):
        self.name = name
        self.svlan1 = svlan1
        self.svlan2 = svlan2
        self.svlan3 = svlan3
        self.cvlan1 = cvlan1
        self.cvlan2 = cvlan2
        self.cvlan3 = cvlan3
        self.gemport1 = gemport1
        self.gemport2 = gemport2
        self.gemport3 = gemport3
        self.ttinternet = ttinternet
        self.ttvoip = ttvoip


    def __repr__(self):
        return '<id {}>'.format(self.id)

    def serialize(self):
        return {
            'id': self.id,
            'svlan1': self.svlan1,
            'svlan2': self.svlan2,
            'svlan3': self.svlan3,
            'cvlan1':self.cvlan1,
            'cvlan2':self.cvlan2,
            'cvlan3':self.cvlan3,
            'gemport1':self.gemport1,
            'gemport2':self.gemport2,
            'gemport3':self.gemport3,
            'ttinternet':self.ttinternet,
            'ttvoip':self.ttvoip
        }

class User(UserMixin, db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))



    def __repr__(self):
        return '<User {}>'.format(self.username) 

    

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)



class ACS(db.Model):
    __tablename__ = 'acs_api'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    ip = db.Column(db.String())
    username = db.Column(db.String())
    password = db.Column(db.String())
    port = db.Column(db.String())


    def __init__(self, name, ip, username, password, port):
        self.name = name
        self.ip = ip
        self.username = username
        self.password = password
        self.port = port
        
    def __repr__(self):
        return '<id {}>'.format(self.id)

    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'ip': self.ip,
            'username': self.username,
            'password':self.password,
            'port':self.tr069,
        }


class TR069Campos(db.Model):
    __tablename__ = 'tr069_api'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    value = db.Column(db.String())


    def __init__(self, name, value):
        self.name = name
        self.value = value
        
        
    def __repr__(self):
        return '<id {}>'.format(self.id)

    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'value': self.value,
        }


class TR069Profile(db.Model):
    __tablename__ = 'tr069_profile'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    acs_id = db.Column(db.Integer, db.ForeignKey('acs_api.id'))
    internet = db.Column(db.String(), nullable=False)
    voip = db.Column(db.String(), nullable=False)


    def __init__(self, name, acs_id, internet, voip):
        self.name = name
        self.acs_id = acs_id
        self.internet = internet
        self.voip = voip
        
        
    def __repr__(self):
        return '<id {}>'.format(self.id)

    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'acs_id': self.acs,
            'internet': self.internet,
            'voip': self.voip,
        }




class TR069ParametersProfile(db.Model):
    __tablename__ = 'tr069_parameters'

    id = db.Column(db.Integer, primary_key=True)
    profile_id = db.Column(db.Integer, db.ForeignKey('tr069_profile.id'))
    campo_id = db.Column(db.Integer, db.ForeignKey('tr069_api.id'))
    value = db.Column(db.String())


    def __init__(self, profile_id, campo_id, value):
        self.profile_id = profile_id
        self.campo_id = campo_id
        self.value = value
        
        
    def __repr__(self):
        return '<id {}>'.format(self.id)

    def serialize(self):
        return {
            'id': self.id,
            'profile_id': self.profile_id,
            'campo_id': self.campo_id,
            'value': self.value,
        }


class SIP(db.Model):
    __tablename__ = 'sip_api'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    ip = db.Column(db.String())
    port = db.Column(db.String())


    def __init__(self, name, ip, port):
        self.name = name
        self.ip = ip
        self.port = port
        

    def __repr__(self):
        return '<id {}>'.format(self.id)

    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'ip': self.ip,
            'port': self.port,
        }
