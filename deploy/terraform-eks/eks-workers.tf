#
# EKS Worker Nodes Resources
#  * IAM role allowing Kubernetes actions to access other AWS services
#  * EKS Node Group to launch worker nodes
#

resource "aws_iam_role" "main-node" {
  name = "terraform-eks-main-node"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "main-node-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.main-node.name
}

resource "aws_iam_role_policy_attachment" "main-node-AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.main-node.name
}

resource "aws_iam_role_policy_attachment" "main-node-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.main-node.name
}

resource "aws_eks_node_group" "ng_public_a" {
  cluster_name    = aws_eks_cluster.main.name
  node_group_name = "ng_public_a"
  node_role_arn   = aws_iam_role.main-node.arn
  subnet_ids      = aws_subnet.public_a[*].id
  labels = {
    label_key = "public"
  }



  scaling_config {
    desired_size = 1
    max_size     = 2
    min_size     = 1
  }

  depends_on = [
    aws_iam_role_policy_attachment.main-node-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.main-node-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.main-node-AmazonEC2ContainerRegistryReadOnly,
  ]
}

resource "aws_eks_node_group" "ng_public_b" {
  cluster_name    = aws_eks_cluster.main.name
  node_group_name = "ng_public_b"
  node_role_arn   = aws_iam_role.main-node.arn
  subnet_ids      = aws_subnet.public_b[*].id
  labels = {
    label_key = "public"
  }


  scaling_config {
    desired_size = 1
    max_size     = 2
    min_size     = 1
  }

  depends_on = [
    aws_iam_role_policy_attachment.main-node-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.main-node-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.main-node-AmazonEC2ContainerRegistryReadOnly,
  ]
}

resource "aws_eks_node_group" "ng_private_a" {
  cluster_name    = aws_eks_cluster.main.name
  node_group_name = "ng_private_a"
  node_role_arn   = aws_iam_role.main-node.arn
  subnet_ids      = aws_subnet.private_a[*].id
  labels = {
    label_key = "private"
  }
  

  scaling_config {
    desired_size = 1
    max_size     = 2
    min_size     = 1
  }

  depends_on = [
    aws_iam_role_policy_attachment.main-node-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.main-node-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.main-node-AmazonEC2ContainerRegistryReadOnly,
  ]
}

resource "aws_eks_node_group" "ng_private_b" {
  cluster_name    = aws_eks_cluster.main.name
  node_group_name = "ng_private_b"
  node_role_arn   = aws_iam_role.main-node.arn
  subnet_ids      = aws_subnet.private_b[*].id
  labels = {
    label_key = "private"
  }
  scaling_config {
    desired_size = 1
    max_size     = 2
    min_size     = 1
  }

  depends_on = [
    aws_iam_role_policy_attachment.main-node-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.main-node-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.main-node-AmazonEC2ContainerRegistryReadOnly,
  ]
}