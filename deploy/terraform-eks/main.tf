terraform {
  backend "s3" {
    bucket         = "myapp-flask-based-stuff-alfa"
    key            = "lask-based-stuff.tfstate"
    region         = "eu-central-1"
    encrypt        = true
    dynamodb_table = "flask-based-stuff-alfa-tf-state-lock"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}



provider "aws" {
  region  = "eu-central-1"
}

data "aws_region" "current" {}



