# Security Group
# EFS and Resources 



resource "aws_security_group" "tf-efs-sg" {
  name        = "tf-efs-sg"
  description = "Communication to EFS"
  vpc_id      = aws_vpc.main.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "tf-efs-sg"
  }
}

resource "aws_efs_file_system" "tf-efs-fs" {
  creation_token = "my-efs-file-system-1"
  performance_mode = "generalPurpose"
  throughput_mode = "bursting"
  encrypted = "true"
  lifecycle_policy {
    transition_to_ia = "AFTER_30_DAYS"
  }
}

data "aws_efs_file_system" "tf-efs-fs" {
  creation_token = "my-efs-file-system-1"
}

resource "aws_efs_mount_target" "tf_efs_mnt_target_a" {
  
  file_system_id = aws_efs_file_system.tf-efs-fs.id
  subnet_id      = aws_subnet.private_a.id
  security_groups = [aws_security_group.tf-efs-sg.id]
}

resource "aws_efs_mount_target" "tf_efs_mnt_target_b" {
  
  file_system_id = aws_efs_file_system.tf-efs-fs.id
  subnet_id      = aws_subnet.private_b.id
  security_groups = [aws_security_group.tf-efs-sg.id]
}

resource "aws_efs_access_point" "tf_efs_access_pt" {
  file_system_id = aws_efs_file_system.tf-efs-fs.id
}


data "aws_availability_zones" "available" {}


resource "kubernetes_storage_class" "tf_efs_sc" {
  metadata {
    name = "tf-eks-sc"
  }
  storage_provisioner = "aws-efs/tf-eks-sc"
  reclaim_policy      = "Retain"
}

### We create the cluster_role_binding for the EFS Provisioning so that it can work on EFS as a PVC.
### A subject is created with the name ServiceAccount and the namespace has to be specified
###

resource "kubernetes_cluster_role_binding" "tf_efs_role_binding" {
   depends_on = [
    kubernetes_namespace.tf-ns,
  ]
  metadata {
    name = "tf_efs_role_binding"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "default"
    namespace = "terraform-prom-graf-namespace"
  }
}


resource "kubernetes_deployment" "tf-efs-provisioner" {

  depends_on = [
  kubernetes_storage_class.tf_efs_sc,
  kubernetes_namespace.tf-ns
  ]

  metadata {
    name = "tf-efs-provisioner"
    namespace = "terraform-prom-graf-namespace"
  }

  spec {
    replicas = 1
    strategy {
      type = "Recreate"
    }
    selector {
      match_labels = {
        app = "tf-efs"
      }
    }

    template {
      metadata {
        labels = {
          app = "tf-efs"
        }
      }

      spec {
        automount_service_account_token = true
        container {
          image = "quay.io/external_storage/efs-provisioner:v0.1.0"
          name  = "tf-efs-provision"
          env {
            name  = "FILE_SYSTEM_ID"
            value = data.aws_efs_file_system.tf-efs-fs.file_system_id
          }
          env {
            name  = "AWS_REGION"
            value = "eu-central-1"
          }
          env {
            name  = "PROVISIONER_NAME"
            value = kubernetes_storage_class.tf_efs_sc.storage_provisioner
          }
          volume_mount {
            name       = "pv-volume"
            mount_path = "/persistentvolumes"
          }
        }
        volume {
          name = "pv-volume"
          nfs {
            server = data.aws_efs_file_system.tf-efs-fs.dns_name
            path   = "/"
          }
        }
      }
    }
  }
}