variable "prefix" {
  default = "haad"
}

variable "project" {
  default = "huawei-app-api-devops"
}

variable "contact" {
  default = "rr@alfa-network.net"
}

variable "db_username" {
  description = "Username for the possible RDS Postgres instance"
}

variable "db_password" {
  description = "Password for the possible RDS postgres instance"
}

variable "aws_region" {
  default = "eu-central-1"
}

variable "cluster-name" {
  default = "terraform-eks-main"
  type    = string
}