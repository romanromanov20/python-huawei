server {
  listen ${LISTEN_PORT};

  location /static {
    alias /vol/static;
  }

  location ~ /.well-known/acme-challenge {
    allow all;
    root /var/www/html;
  }

  location / { 
    proxy_pass ${APP_HOST}:${APP_PORT}/;
    proxy_set_header Host $host:$server_port; 
    proxy_set_header X-Real-IP $remote_addr; 
	}

}

