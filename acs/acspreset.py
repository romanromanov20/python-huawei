import requests

def (acsurl, apiport, model, serial, presetlines):


    id = '00259E-'+model+'-'+serial
    requestadd = 'http://'+acsurl+':'+apiport+'/presets/'+id
    data = '{ "weight" : 0, "precondition" : "{\\"_id\\":\\"'+id+'\\",\\"_tags\\":\\"WAN_OK\\"}", "configurations" : [ '+presetlines+']  }'
    response = requests.put(requestadd, data=data)
    print(response)


