import requests

model='HG8245H'
serial='48575443D950345A'
acsurl='127.0.0.1'
apiport='7557'

wifi = '{ "type" : "value", "name" : "InternetGatewayDevice.LANDevice.1.WLANConfiguration.1.SSID", "value" : "'+ssid+'" }'


id = '00259E-'+model+'-'+serial
requestadd = 'http://'+acsurl+':'+apiport+'/presets/'+id

data = '{ "weight" : 0, "precondition" : "{\\"_id\\":\\"'+id+'\\",\\"_tags\\":\\"WAN_OK\\"}", "configurations" : \
[ '+wifi+' \
{ "type" : "value", "name" : "InternetGatewayDevice.X_HW_Security.WANSrcWhiteList.WANSrcWhiteListEnable", "value" : "true" }, \
{ "type" : "value", "name" : "InternetGatewayDevice.X_HW_Security.WANSrcWhiteList.List.1.SrcIPPrefix", "value" : "185.19.190.10/32" }, \
{ "type" : "value", "name" : "InternetGatewayDevice.WANDevice.1.WANConnectionDevice.2.WANPPPConnection.1.Password", "value" : "4Pz8J76C" }, \
{ "type" : "value", "name" : "InternetGatewayDevice.UserInterface.X_HW_WebUserInfo.2.UserName", "value" : "scansat" },\
{ "type" : "value", "name" : "InternetGatewayDevice.ManagementServer.PeriodicInformInterval", "value" : "86400" }, \
{ "type" : "value", "name" : "InternetGatewayDevice.LANDevice.1.LANEthernetInterfaceConfig.1.X_HW_L3Enable", "value" : "1" }, \
{ "type" : "value", "name" : "InternetGatewayDevice.LANDevice.1.LANEthernetInterfaceConfig.2.X_HW_L3Enable", "value" : "1" }, \
{ "type" : "value", "name" : "InternetGatewayDevice.LANDevice.1.LANEthernetInterfaceConfig.3.X_HW_L3Enable", "value" : "1" }, \
{ "type" : "value", "name" : "InternetGatewayDevice.LANDevice.1.LANEthernetInterfaceConfig.4.X_HW_L3Enable", "value" : "1" }, \
{ "type" : "value", "name" : "InternetGatewayDevice.WANDevice.1.WANConnectionDevice.2.WANPPPConnection.1.NATEnabled", "value" : "1" }, \
{ "type" : "value", "name" : "InternetGatewayDevice.WANDevice.1.WANConnectionDevice.2.WANPPPConnection.1.X_HW_PRI", "value" : "5" }, \
{ "type" : "value", "name" : "InternetGatewayDevice.LANDevice.1.WLANConfiguration.1.RegulatoryDomain", "value" : "ES" }, \
{ "type" : "value", "name" : "InternetGatewayDevice.UserInterface.X_HW_CLIUserInfo.1.Username", "value" : "scansat" }, \
{ "type" : "value", "name" : "InternetGatewayDevice.ManagementServer.ConnectionRequestUsername", "value" : "scansat" }, \
{ "type" : "value", "name" : "InternetGatewayDevice.ManagementServer.ConnectionRequestPassword", "value" : "ScanSatPassword" }, \
{ "type" : "value", "name" : "InternetGatewayDevice.WANDevice.1.WANConnectionDevice.2.WANPPPConnection.1.PPPLCPEcho", "value" : "10" }, \
{ "type" : "value", "name" : "InternetGatewayDevice.WANDevice.1.WANConnectionDevice.2.WANPPPConnection.1.PPPLCPEchoRetry", "value" : "3" }, \
{ "type" : "value", "name" : "InternetGatewayDevice.WANDevice.1.WANConnectionDevice.2.ConnectionType", "value" : "IP_Routed" }, \
{ "type" : "value", "name" : "InternetGatewayDevice.WANDevice.1.WANConnectionDevice.2.WANPPPConnection.1.X_HW_VLAN", "value" : "1001" }, \
{ "type" : "value", "name" : "InternetGatewayDevice.UserInterface.X_HW_WebUserInfo.2.Password", "value" : "ScanSatPassword" }, \
{ "type" : "value", "name" : "InternetGatewayDevice.UserInterface.X_HW_CLIUserInfo.1.Userpassword", "value" : "ScanSatPassword" }, \
{ "type" : "value", "name" : "InternetGatewayDevice.WANDevice.1.WANConnectionDevice.2.WANPPPConnection.1.Username", "value" : "43117680B54949565@" }, \
{ "type" : "value", "name" : "InternetGatewayDevice.LANDevice.1.WLANConfiguration.1.PreSharedKey.1.PreSharedKey", "value" : "865529465" }, \
{ "type" : "value", "name" : "InternetGatewayDevice.X_HW_Security.AclServices.HTTPWanEnable", "value" : "0" } ]  }'

response = requests.put(requestadd, data=data)

response = str(response)

print(response)