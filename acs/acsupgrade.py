import requests
from models import *
from sqlalchemy import text


def acspresetupgrade(acsurl, apiport, model, serial, ssid, ssid5g, wifipass, pppuser, ppppass, tlf, tlfpass, profile_id):
    id = '00259E-'+model+'-'+serial
    requestadd = 'http://'+acsurl+':'+apiport+'/presets/'+id
    requestrefresh = 'http://'+acsurl+':'+apiport+'/devices/'+id+'/tasks?connection_request'
    refreshdata='{"name":"refreshObject", "objectName":""}'


    wifi = '{ "type" : "value", "name" : "InternetGatewayDevice.LANDevice.1.WLANConfiguration.1.SSID", "value" : "'+ssid+'" }'
    wifipass = '{ "type" : "value", "name" : "InternetGatewayDevice.LANDevice.1.WLANConfiguration.1.PreSharedKey.1.PreSharedKey", "value" : "'+wifipass+'" }'
    pppoeuser = '{ "type" : "value", "name" : "InternetGatewayDevice.WANDevice.1.WANConnectionDevice.2.WANPPPConnection.1.Username", "value" : "'+pppuser+'" }'
    ppppassword = '{ "type" : "value", "name" : "InternetGatewayDevice.WANDevice.1.WANConnectionDevice.2.WANPPPConnection.1.Password", "value" : "'+ppppass+'" } '


    sql=text('select c.name as cname, p.value as pvalue from tr069_api c, tr069_parameters p where p.campo_id = c.id and p.profile_id=:profile_id;')
    tr069profiles = db.engine.execute(sql, profile_id=profile_id )
    resultstring = ''
    for line in tr069profiles:
        mystring = '{ "type" : "value", "name" : "' + line.cname + '", "value" : "' + line.pvalue + '" }, '
        resultstring = resultstring + mystring
    
    


    id = '00259E-'+model+'-'+serial
    requestadd = 'http://'+acsurl+':'+apiport+'/presets/'+id

    data = '{ "weight" : 0, "precondition" : "{\\"_id\\":\\"'+id+'\\",\\"_tags\\":\\"WAN_OK\\"}", "configurations" : [ '+resultstring+' '+wifi+', '+wifipass+', '+pppoeuser+', '+ppppassword+']  }'

    response = requests.put(requestadd, data=data)

    response2 = requests.post(requestrefresh, data=refreshdata)

    #print(requestadd)
    #print(data)
    #print(response.json())

    return response