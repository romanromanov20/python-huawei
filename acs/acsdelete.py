import requests

def acspresetdelete(acsurl, apiport, model, serial):
    id = '00259E-'+model+'-'+serial
    requestt = 'http://'+acsurl+':'+apiport+'/presets/'+id
    resp = requests.delete(requestt)
    resp = str(resp)
    return(resp)

def acsdevicedelete(acsurl, apiport, model, serial):
    id = '00259E-'+model+'-'+serial
    requestt = 'http://'+acsurl+':'+apiport+'/devices/'+id
    resp = requests.delete(requestt)
    resp = str(resp)
    return(resp)
