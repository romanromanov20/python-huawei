while ! nc -z psql 5432; do
  sleep 0.1
done

echo "Postgresql started"

flask db init
flask db migrate
flask db upgrade
exec app.py
