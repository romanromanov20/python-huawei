#!/usr/bin/env python
import paramiko, time, sys




def olt_remove(router, username, password, sn_number):
	ssh = paramiko.SSHClient()
	ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	ssh.connect(router, username=username, password=password,allow_agent=False, look_for_keys=False)
#	print('Successfully connected to %s' % router)

	remote_conn = ssh.invoke_shell()
	output = remote_conn.recv(1000)


	remote_conn.send('enable\n')
	remote_conn.send('config\n')
	remote_conn.send('scroll 512\n')
	remote_conn.send('display ont info by-sn '+ sn_number + ' \n')
	remote_conn.send(' \n')
	time.sleep(2)
	outpt = remote_conn.recv(9999)
	output = outpt.decode()

	for line in output.split('\n'):
		if "The required ONT does not exist" in line:
			print("The required ONT does not exist!")
			result = '{"No ONT found"}'
			return result
			sys.exit()
		if "ONT-ID" in line:
			str_onuid = line.split(':')
			line_id = str_onuid[1].lstrip().rstrip('\r')
		if "F/S/P" in line:
			str_onuid = line.split(':')
			line2_id = str_onuid[1].lstrip().rstrip('\r')
			slot_id = line2_id.split('/')[1]
			port_id = line2_id.split('/')[2]
		if "Run state" in line:
			str_state = line.split(':')
			run_state = str_state[1].lstrip().rstrip('\r')
			print('State: '+ run_state)
	try:
		print('slot id ='+ slot_id + ' port id = ' + port_id + ' ONT ID = '+ line_id)
		remote_conn.send('display service-port board  0/'+ slot_id +' \n')
		remote_conn.send(' \n')
		time.sleep(3)
		remote_conn.send(' \n')
		time.sleep(3)
		service_output = remote_conn.recv(99999)
		output_n = service_output.decode()

		for line in output_n.split('\n'):
			if ' 0/'+ str(slot_id) +' /'+ str(port_id) +' '+ str(line_id) in line:

				service_id = line.split()[0]
				print('undo service-port '+service_id+' \n')
				#remote_conn.send('undo service-port '+service_id+' \n')  
				remote_conn.send(' \n')


		#remote_conn.send('interface gpon 0/'+ slot_id +' \n')
		print('ont delete '+ port_id +' '+ line_id +' \n')
		#remote_conn.send('ont delete '+ port_id +' '+ line_id +' \n')
		remote_conn.send(' \n')
		print('ONT removed '+ sn_number)

	except Exception as e:
		print(e)
		result = '{"No ONT found, produced error"}'
		return result
	ssh.close()
	if line_id:
		result = '{"ONT_removed" : "'+ sn_number +'" }'
	else:
		result = '{"No ONT found, error no ont id found"}'
	return result


if __name__ == '__main__':
	
	olt_remove(router, username, password, sn_number)
