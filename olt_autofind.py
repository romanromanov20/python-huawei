#!/usr/bin/env python
import paramiko, time, sys


def autofind_func(router, username, password):
	stringg = ''
	stringg1 = ''
	d = {'ont_sn': [], 'slot_id': [], 'port_id': [], 'version': []}

	ssh = paramiko.SSHClient()
	try:
		ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		ssh.connect(router, username=username, password=password,allow_agent=False, look_for_keys=False, timeout=5)
		remote_conn = ssh.invoke_shell()
		output = remote_conn.recv(1000)
		remote_conn.send('enable\n')
		remote_conn.send('display ont autofind all\n')
		remote_conn.send(' \n')
		time.sleep(1)
		outpt = remote_conn.recv(5000)
		output = outpt.decode()
		for line in output.split('\n'):
			if "F/S/P" in line:
				str_onuid = line.split(':')
				line2_id = str_onuid[1].lstrip().rstrip('\r')
				slot_id = line2_id.split('/')[1]
				port_id = line2_id.split('/')[2]
				d['slot_id'] = slot_id
				d['port_id'] = port_id
			if "Ont SN" in line:
				str_onuid = line.split(':')
				ont_sn = str_onuid[1].lstrip().rstrip('\r')
				d['ont_sn'] = ont_sn
			if "Ont EquipmentID" in line:
				str_onuid = line.split(':')
				vers = str_onuid[1].lstrip().rstrip('\r')
				d['version'] = vers
				stringg1 =str(d)
				stringg =stringg+','+stringg1 
			if "Failure: The automatically found ONTs do not exis" in line:
				stringg = '"No_ONTs_to_add"'
		ssh.close()
		stringg1 = stringg.replace("\'", "\"")
		stringg1 = stringg1.replace(",", " ", 1)
		stringg1 = '{ "onts": [ '+ stringg1 +'] }'
	except:
		stringg1 = 'Timeout'
	return stringg1

if __name__ == '__main__':
	autofind_func(router, username, password)

